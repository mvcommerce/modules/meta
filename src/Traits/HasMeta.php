<?php

namespace MVCommerceModules\Meta\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use MVCommerceModules\Meta\Meta;

/**
 * Trait HasMeta
 * @package MVCommerceModules\Meta
 *
 * @property Collection $meta Meta data in key => value format. Setting this variable will replace set the value in database automatically.
 */
trait HasMeta
{


    protected $_metaCached;


    // Relations ========================

    /**
     * @return MorphMany
     */
    public function meta(){
        return $this->morphMany( Meta::class, 'object' );
    }



    // Scopes ===========================

    public function scopeMetaQuery(Builder $query, array $metaQuery){

        $query->whereHas( 'meta', function(Builder $query) use ($metaQuery){

            $query->where(function(Builder $query) use ($metaQuery){

                foreach ($metaQuery as $item){

                    if( count($item) === 2 ){
                        list($key, $value) = $item;
                        $operator = '=';
                    }else{
                        list($key, $operator, $value) = $item;
                    }

                    $query->orWhere(function(Builder $query) use ($key, $operator, $value){
                        $query->where('key', $key);
                        $query->where('value', $operator, $value);
                    });

                }

            });

            $query->groupBy('object_id');
            $query->select(DB::raw('COUNT(*)'));
            $query->having(DB::raw('COUNT(*)'), '>=', count($metaQuery));

        });
    }



    // Methods =========================

    protected function _fetchMetaCacheFromDB(){
        return $this->_metaCached = $this->meta()->pluck('value', 'key');
    }



    // Accessors =======================

    public function getMetaAttribute(){

        if( $this->_metaCached ){
            return $this->_metaCached;
        }

        return $this->_fetchMetaCacheFromDB();

    }



    // Mutators =======================

    /**
     * @param array $values
     * @return Collection
     */
    public function setMetaAttribute(array $values){


        $metaArr = $this->meta->toArray();
        $class_name = get_class($this);
        $object_id = $this->{$this->getKeyName()};

        // dd($metaArr, $values, array_diff_key($values, $metaArr));

        // Delete non-necessary keys.
        $diff = array_diff_key($metaArr, $values);
        if(!empty($diff)){
            $this->meta()->whereIn('key', array_keys($diff))->delete();
        }


        // Update existing keys.
        $common = array_intersect_key($values, $metaArr);
        if(!empty($common)){
            foreach ($common as $key => $val){

                // Skip update if it's already set.
                if( $metaArr[$key] == $val ) continue;

                $this->meta()->where('key', $key)->update(['value' => $val]);
            }
        }


        // Insert new keys.
        $new = array_diff_key($values, $metaArr);
        if(!empty($new)){

            array_walk($new, function(&$val, $key) use ( $class_name, $object_id ){
                $val = [
                    'key' => $key,
                    'value' => $val,
                    'object_type' => $class_name,
                    'object_id' => $object_id,
                ];
            });

            $new = array_values($new);
            $this->meta()->insert($new);

        }

        return $this->_fetchMetaCacheFromDB();

    }

}
