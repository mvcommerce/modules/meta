<?php

namespace MVCommerceModules\Meta\Providers;


use Illuminate\Support\ServiceProvider;
use MVCommerceModules\Meta\Controllers\MetaController;

class MetaServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], ['mvcommerce', 'mvcommerce_meta', 'mvcommerce_meta_migrations', 'migrations']);

    }


    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {



    }


}
