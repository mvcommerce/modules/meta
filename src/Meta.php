<?php

namespace MVCommerceModules\Meta;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $key
 * @property string $value
 * @property HasMeta $object
 */
class Meta extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'key', 'value'
    ];


    // Relations ========================

    public function object(){
        return $this->morphTo();
    }

}
